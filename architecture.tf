# These are set from "TF_" prefixed .env vars.
variable "SOURCE_BUCKET" {
    default = "hipaa-zappa-source-bucket-changeme"
}
variable "DYNAMO_TABLE_NAME" {
  default = "zappa-responses"
}
variable "REGION" {
  default = "us-east-1"
}
variable "STAGE" {
  default = "dev"
}

provider "aws" {
    # access_key = "${var.access_key}"
    # secret_key = "${var.secret_key}"
    region = "${var.REGION}"
}

# Create IAM Roles
resource "aws_iam_role" "iam_for_hipaa_lambda" {
  name = "iam_for_hipaa_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": [
          "apigateway.amazonaws.com",
          "lambda.amazonaws.com",
          "events.amazonaws.com"
        ]
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "policy_for_hipaa_lambda" {
  name = "iam_for_hipaa_lambda_policy"
  role = "${aws_iam_role.iam_for_hipaa_lambda.id}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "logs:*"
            ],
            "Resource": "arn:aws:logs:*:*:*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "lambda:InvokeFunction"
            ],
            "Resource": [
                "*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "ec2:AttachNetworkInterface",
                "ec2:CreateNetworkInterface",
                "ec2:DeleteNetworkInterface",
                "ec2:DescribeInstances",
                "ec2:DescribeNetworkInterfaces",
                "ec2:DetachNetworkInterface",
                "ec2:ModifyNetworkInterfaceAttribute",
                "ec2:ResetNetworkInterfaceAttribute"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:*"
            ],
            "Resource": "arn:aws:s3:::*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "kinesis:*"
            ],
            "Resource": "arn:aws:kinesis:*:*:*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "sns:*"
            ],
            "Resource": "arn:aws:sns:*:*:*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "sqs:*"
            ],
            "Resource": "arn:aws:sqs:*:*:*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "dynamodb:*"
            ],
            "Resource": "arn:aws:dynamodb:*:*:*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "route53:*"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

##
# Network
##

resource "aws_cloudformation_stack" "network" {
  name = "networking-stack"
  template_body = "${file("network.yml")}"
}

##
# Function / Packaging
##

# Fill in our settings template
data "template_file" "zappa_template" {
  template = "${file("zappa_settings.tpl")}"

  vars {
    bucket_name = "${var.SOURCE_BUCKET}"
    subnet1 = "${aws_cloudformation_stack.network.outputs["PrivateSubnet1"]}"
    subnet2 = "${aws_cloudformation_stack.network.outputs["PrivateSubnet2"]}"
    subnet3 = "${aws_cloudformation_stack.network.outputs["PrivateSubnet3"]}"
    security_group_id = "${aws_cloudformation_stack.network.outputs["SecurityGroupId"]}"
    dynamo_table_name = "${var.DYNAMO_TABLE_NAME}"
  }
}

# data "template_file" "zappa_template" {
#   template = "${file("zappa_settings.tpl")}"

#   vars {
#     bucket_name = "${var.SOURCE_BUCKET}"
#     dynamo_table_name = "${var.DYNAMO_TABLE_NAME}"
#     subnet1 = "derp"
#     subnet2 = "derp"
#     subnet3 = "derp"
#   }
# }

# Render it
resource "local_file" "zappa_settings_file" {
  content  = "${data.template_file.zappa_template.rendered}"
  filename = "zappa_settings.json"
}

# Create our package
resource "null_resource" "zappa_package" {
  depends_on = ["local_file.zappa_settings_file"]
  provisioner "local-exec" {
    command = "zappa package -o z-pack.zip"
  }
}

# Create our Buckets
resource "aws_s3_bucket" "source_bucket" {
  bucket = "${var.SOURCE_BUCKET}"
  acl    = "private"
}

# Upload the Package to S3
resource "aws_s3_bucket_object" "zip_object" {
  bucket = "${aws_s3_bucket.source_bucket.bucket}"
  key    = "z-pack.zip"
  source = "z-pack.zip"
  depends_on = ["null_resource.zappa_package", "aws_s3_bucket.source_bucket"]
}

# Register the Lambda Function
resource "aws_lambda_function" "hipaa_lambda" {
  filename = "z-pack.zip"
  # source_code_hash = "${data.archive_file.lambda_zip.output_base64sha256}"
  function_name = "hipaa_lambda"
  depends_on = ["aws_iam_role.iam_for_hipaa_lambda", "aws_s3_bucket_object.zip_object"]
  role = "${aws_iam_role.iam_for_hipaa_lambda.arn}"
  description = "Zappa HIPAA AWS Lambda"
  handler = "handler.lambda_handler"
  runtime = "python2.7"
  memory_size = "512"
  vpc_config {
    subnet_ids         = ["${aws_cloudformation_stack.network.outputs["PrivateSubnet1"]}", "${aws_cloudformation_stack.network.outputs["PrivateSubnet2"]}", "${aws_cloudformation_stack.network.outputs["PrivateSubnet3"]}"]
    security_group_ids = ["${aws_cloudformation_stack.network.outputs["SecurityGroupId"]}"]
  }

}

# Create our API Gateway template
# I'd much rather use this than use the static template below,
# but terraform doesn't like that idea:
# Related ticket: https://github.com/hashicorp/terraform/issues/13991

# data "external" "template" {
#   # program =  ["zappa", "template",  "-o z-template.json", "-r '${aws_iam_role.iam_for_hipaa_lambda.arn}'", "-l '${aws_lambda_function.hipaa_lambda.arn}'", "--json"]
#   program =  ["zappa", "template",  "-r '${aws_iam_role.iam_for_hipaa_lambda.arn}'", "-l '${aws_lambda_function.hipaa_lambda.arn}'", "--json"]
#   depends_on = ["aws_lambda_function.hipaa_lambda"]
# }

#Create the API Gateway via CloudFormation
resource "aws_cloudformation_stack" "apigw" {
  name = "hipaa-apigw-stack"
  depends_on = ["aws_iam_role.iam_for_hipaa_lambda"]

  # See comment above.
  #template_body = "${data.external.template.result}"

  template_body = <<EOF
{
  "Outputs": {
    "RestApiId": {
      "Description": "Information about the value",
      "Value": {"Ref": "Api"},
      "Export": {
        "Name": "RestApiId"
      }
    }
  },
  "Description": "Automatically generated with Zappa",
  "Resources": {
    "ANY0": {
      "Properties": {
        "ApiKeyRequired": false,
        "AuthorizationType": "NONE",
        "HttpMethod": "ANY",
        "Integration": {
          "CacheKeyParameters": [],
          "CacheNamespace": "none",
          "Credentials": "${aws_iam_role.iam_for_hipaa_lambda.arn}",
          "IntegrationHttpMethod": "POST",
          "IntegrationResponses": [],
          "PassthroughBehavior": "NEVER",
          "Type": "AWS_PROXY",
          "Uri": "arn:aws:apigateway:us-east-1:lambda:path/2015-03-31/functions/${aws_lambda_function.hipaa_lambda.arn}/invocations"
        },
        "MethodResponses": [],
        "ResourceId": {
          "Fn::GetAtt": ["Api", "RootResourceId"]
        },
        "RestApiId": {
          "Ref": "Api"
        }
      },
      "Type": "AWS::ApiGateway::Method"
    },
    "ANY1": {
      "Properties": {
        "ApiKeyRequired": false,
        "AuthorizationType": "NONE",
        "HttpMethod": "ANY",
        "Integration": {
          "CacheKeyParameters": [],
          "CacheNamespace": "none",
          "Credentials": "${aws_iam_role.iam_for_hipaa_lambda.arn}",
          "IntegrationHttpMethod": "POST",
          "IntegrationResponses": [],
          "PassthroughBehavior": "NEVER",
          "Type": "AWS_PROXY",
          "Uri": "arn:aws:apigateway:us-east-1:lambda:path/2015-03-31/functions/${aws_lambda_function.hipaa_lambda.arn}/invocations"
        },
        "MethodResponses": [],
        "ResourceId": {
          "Ref": "ResourceAnyPathSlashed"
        },
        "RestApiId": {
          "Ref": "Api"
        }
      },
      "Type": "AWS::ApiGateway::Method"
    },
    "Api": {
      "Properties": {
        "Description": "Created automatically by Zappa.",
        "Name": "hipaa-api"
      },
      "Type": "AWS::ApiGateway::RestApi"
    },
    "ResourceAnyPathSlashed": {
      "Properties": {
        "ParentId": {
          "Fn::GetAtt": ["Api", "RootResourceId"]
        },
        "PathPart": "{proxy+}",
        "RestApiId": {
          "Ref": "Api"
        }
      },
      "Type": "AWS::ApiGateway::Resource"
    }
  }
}
EOF
}

output "apigw" {
  value = "${aws_cloudformation_stack.apigw.outputs}"
}

# Deploy the API GW Stage
resource "aws_api_gateway_deployment" "deployment" {
  depends_on = ["aws_cloudformation_stack.apigw"]
  rest_api_id = "${aws_cloudformation_stack.apigw.outputs["RestApiId"]}"
  stage_name = "${var.STAGE}"
}

# Create the HIPAA distro
resource "aws_cloudfront_distribution" "hipaa_distribution" {
  origin {
    domain_name = "${aws_cloudformation_stack.apigw.outputs["RestApiId"]}.execute-api.${var.REGION}.amazonaws.com"
    origin_id   = "hipaa_origin"
    custom_origin_config = {
      http_port = 80
      https_port = 443
      origin_protocol_policy = "match-viewer"
      origin_ssl_protocols = ["TLSv1.2"]
      }
  }

  enabled             = true
  is_ipv6_enabled     = true
  comment             = ""
  default_root_object = ""
  price_class= "PriceClass_100"

  default_cache_behavior {
    allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "hipaa_origin"

    forwarded_values {
      query_string = true
      headers = []
      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "https-only"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    cloudfront_default_certificate = true
  }
}

output "hipaa_distribution" {
  value = "${aws_cloudfront_distribution.hipaa_distribution.outputs}"
}

##
# DB
##

resource "aws_dynamodb_table" "basic-dynamodb-table" {
  name           = "${var.DYNAMO_TABLE_NAME}"
  read_capacity  = 5
  write_capacity = 5
  hash_key       = "id"

  attribute {
    name = "id"
    type = "S"
  }

  ttl {
    attribute_name = "TimeToExist"
    enabled = false
  }

  tags {
    Name        = ""
    Environment = ""
  }
}

