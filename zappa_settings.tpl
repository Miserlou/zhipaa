{
    "dev": {
        "aws_region": "us-east-1",
        "app_function": "app.app",
        "profile_name": "default",
        "s3_bucket": "${bucket_name}",
	    "binary_support": false,
        "async_resources": false,
        "async_response_table": "${dynamo_table_name}",
        "async_response_table_read_capacity": 1,
        "async_response_table_write_capacity": 1,
        "vpc_config" : {
            "SubnetIds": [
                "${subnet1}",
                "${subnet2}",
                "${subnet3}",
            ],
            "SecurityGroupIds": ["${security_group_id}"]
        }
    }
}
