# ZHIPAA

A template for a HIPAA-compliant server-less application using Zappa and CloudFormation.

This creates:

* A VPC With Public/Private Subnets
* NAT Gateway
* VPC Endpoints for DynamoDB
* A DynamoDB Table For Async Responses
* HIPAA Compliant CloudFront Distribution in front of the API Gateway

You will need to modify this script to fit your own application, but this will serve as a good starting point for you.

By default, the way to use this is with `terraform`, however, you could also deploy the individual pieces with `awscli` and `zappa` alone. As I mentioned,
you will need to modify this template to suit the needs and requirements of your application.

## Creation

The infrastructure scripts are written for Terraform in HashiCorp Configuration Language (HCL).

To use these scripts, you will need `terraform` installed.

If you are on OSX, you can install `terraform` with `brew` like so:

    $ brew install terraform

For other operating systems, you should check the [Terraform Downloads Page](https://www.terraform.io/downloads.html).

Next, initialize your environment like so:

    $ terraform init

Then, make sure the template is valid like so:

    $ terraform plan

And finally, when you're ready to rock:

    $ terraform apply

You should now see the assets in your AWS console.

## Updating

The process for updating the code is as simple, just

    $ terraform plan; terraform apply;

this will update the dynamic assets, but leave the existing VPC infrastructure.

## Destruction
    
Destroying the environtment is as simple as:

    $ terraform deploy

and then typing 'yes' to confirm this destruction.

Sometimes, because the networking stack takes so long to destroy, it times out/gets stuck on the final piece. You can remove this manually in the AWS console.

## Troubleshooting

If you are unable to `apply` or `destroy`, you may need to delete the stack from your CloudFormation console in AWS.
