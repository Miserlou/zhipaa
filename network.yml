Description: >
  A sample VPC with NAT. Use this if you need both
  private and public access to resources from lambda, etc.
  The cost is around $30-40 a month (NAT is most of this).
  Important - there is only one NAT in one AZ. In production
  you should probably have two NATs in two AZs for redundancy.

Parameters:
  VpcIpBlock:
    Description: CIDR block for the VPC
    Type: String
    Default: 172.0.0.0/16
  SubnetIpBlocks:
    Description: Comma-delimited list of four CIDR blocks
    Type: CommaDelimitedList
    Default: '172.0.0.0/20, 172.0.16.0/20, 172.0.32.0/20, 172.0.48.0/20'
  SubnetAzs:
    Description: Comma-delimited list of at least four Availability Zones
    Type: CommaDelimitedList
    Default: 'a, b, c, d'

Resources:
  ServiceVpc:
    Type: AWS::EC2::VPC
    Properties:
      CidrBlock: !Ref VpcIpBlock
      EnableDnsSupport: true
      EnableDnsHostnames: true
      Tags:
      - Key: Name
        Value: !Sub "${AWS::StackName}-vpc"

  # Anything in this subnet with a public IP will
  # have internet access as well as private resource
  # access within the VPC.
  # External resources can also access instances in
  # this subnet via the public IP.
  PublicSubnet:
    Type: AWS::EC2::Subnet
    Properties:
      AvailabilityZone: !Sub
      - "${AWS::Region}${AZ}"
      - AZ: !Select [0, !Ref SubnetAzs]
      VpcId: !Ref ServiceVpc
      CidrBlock: !Select [0, !Ref SubnetIpBlocks]
      Tags:
      - Key: Name
        Value: !Sub "${AWS::StackName}-public"

  # Anything in the private subnets will have access
  # to private resources as well as public resources
  # through the NAT. Since it operates behind a NAT,
  # there is no way to access resources in these from
  # outside the VPC, by design.
  PrivateSubnet1:
    Type: AWS::EC2::Subnet
    Properties:
      AvailabilityZone: !Sub
      - "${AWS::Region}${AZ}"
      - AZ: !Select [1, !Ref SubnetAzs]
      VpcId: !Ref ServiceVpc
      CidrBlock: !Select [1, !Ref SubnetIpBlocks]
      Tags:
      - Key: Name
        Value: !Sub "${AWS::StackName}-private1"

  PrivateSubnet2:
    Type: AWS::EC2::Subnet
    Properties:
      AvailabilityZone: !Sub
      - "${AWS::Region}${AZ}"
      - AZ: !Select [2, !Ref SubnetAzs]
      VpcId: !Ref ServiceVpc
      CidrBlock: !Select [2, !Ref SubnetIpBlocks]
      Tags:
      - Key: Name
        Value: !Sub "${AWS::StackName}-private2"

  PrivateSubnet3:
    Type: AWS::EC2::Subnet
    Properties:
      AvailabilityZone: !Sub
      - "${AWS::Region}${AZ}"
      - AZ: !Select [3, !Ref SubnetAzs]
      VpcId: !Ref ServiceVpc
      CidrBlock: !Select [3, !Ref SubnetIpBlocks]
      Tags:
      - Key: Name
        Value: !Sub "${AWS::StackName}-private3"

  PublicGateway:
    # The internet gateway routes traffic to
    # the public internet from a subnet.
    # The resources in the subnet must have public
    # IPs for this to work.
    Type: AWS::EC2::InternetGateway
    Properties:
      Tags:
      - Key: Name
        Value: !Sub "${AWS::StackName}-igw"

  PublicGatewayAttachment:
    Type: AWS::EC2::VPCGatewayAttachment
    Properties:
      InternetGatewayId: !Ref PublicGateway
      VpcId: !Ref ServiceVpc

  PublicRouteTable:
    Type: AWS::EC2::RouteTable
    Properties:
      VpcId: !Ref ServiceVpc
      Tags:
      - Key: Name
        Value: !Sub "${AWS::StackName}-public"

  PublicInternetRoute:
    Type: AWS::EC2::Route
    DependsOn: PublicGatewayAttachment
    Properties:
      RouteTableId: !Ref PublicRouteTable
      DestinationCidrBlock: 0.0.0.0/0
      GatewayId: !Ref PublicGateway

  PublicSubnetRoutes:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      RouteTableId: !Ref PublicRouteTable
      SubnetId: !Ref PublicSubnet

  PrivateNatGateway:
    Type: AWS::EC2::NatGateway
    Properties:
      AllocationId: !GetAtt [ NatEip, AllocationId ]
      SubnetId: !Ref PublicSubnet

  NatEip:
    Type: AWS::EC2::EIP
    Properties:
      Domain: vpc

  PrivateRouteTable:
    Type: AWS::EC2::RouteTable
    Properties:
      VpcId: !Ref ServiceVpc
      Tags:
      - Key: Name
        Value: !Sub "${AWS::StackName}-private"

  PrivateNatRoute:
    Type: AWS::EC2::Route
    Properties:
      RouteTableId: !Ref PrivateRouteTable
      DestinationCidrBlock: 0.0.0.0/0
      NatGatewayId: !Ref PrivateNatGateway

  PrivateSubnetRoutes1:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      RouteTableId: !Ref PrivateRouteTable
      SubnetId: !Ref PrivateSubnet1

  PrivateSubnetRoutes2:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      RouteTableId: !Ref PrivateRouteTable
      SubnetId: !Ref PrivateSubnet2

  PrivateSubnetRoutes3:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      RouteTableId: !Ref PrivateRouteTable
      SubnetId: !Ref PrivateSubnet3

  DynamoDBEndpoint:
    Type: "AWS::EC2::VPCEndpoint"
    Properties:
      RouteTableIds:
        - !Ref PublicRouteTable
        - !Ref PrivateRouteTable
      ServiceName:
        !Sub "com.amazonaws.${AWS::Region}.dynamodb"
      VpcId: !Ref ServiceVpc

Outputs:
  StackVPC:
    Description: The ID of the created VPC
    Value: !Ref ServiceVpc
    Export:
      Name: !Sub "${AWS::StackName}-VPCID"
  PublicSubnet:
    Description: The ID of the public subnet
    Value: !Ref PublicSubnet
    Export:
      Name: !Sub "${AWS::StackName}-PublicSubnet"
  PrivateSubnets:
    Description: Comma separated IDs of the private subnets
    Export:
      Name: !Sub "${AWS::StackName}-PrivateSubnets"
    Value: !Join
    - ","
    - - !Ref PrivateSubnet1
      - !Ref PrivateSubnet2
      - !Ref PrivateSubnet3
  PrivateSubnet1:
    Description: PrivateSubnet1 ID
    Export:
      Name: !Sub "${AWS::StackName}-PrivateSubnet1"
    Value: !Ref PrivateSubnet1
  PrivateSubnet2:
    Description: PrivateSubnet2 ID
    Export:
      Name: !Sub "${AWS::StackName}-PrivateSubnet2"
    Value: !Ref PrivateSubnet2
  PrivateSubnet3:
    Description: PrivateSubnet3 ID
    Export:
      Name: !Sub "${AWS::StackName}-PrivateSubnet3"
    Value: !Ref PrivateSubnet3
  SecurityGroupId:
    Description: SG ID
    Value: !GetAtt [ ServiceVpc, DefaultSecurityGroup ]
    Export:
      Name: !Sub "${AWS::StackName}-SecurityGroupId"
